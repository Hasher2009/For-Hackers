import socket

try:
    HOST = '127.0.0.1' # '192.168.43.82'
    PORT = 8081 # 2222
    server = socket.socket()
    server.bind((HOST, PORT))
    print('[+] Server Started')
    print('[+] Listening For Client Connection ...')
    server.listen(1)
    client, client_addr = server.accept()
    print(f'[+] {client_addr} Client connected to the server')
except KeyboardInterrupt:
    print("[-] Keyboard Interrupt")
    quit()

while True:
    try:
        command = input('[>] Enter Command : ')
        if command == "quit" or command == "exit":
            quit()
        elif command == "":
            continue
        else:
            command = command.encode()
            client.send(command)
            print('[+] Command sent')
            output = client.recv(1024)
            output = output.decode()
            print(f"[+] Output: \n\n{output}")
    except KeyboardInterrupt:
        quit()